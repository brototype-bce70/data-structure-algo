

class Box{
    constructor(data){
        this.data = data
        this.next = null
    }
}

class Shelf{
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }
    
    // Big-O : O(1)t
    addStart(data){
        const node = new Box(data)

        if(this.head === null){
            this.head = node
            this.tail = node
            this.size++
        }else{
            node.next = this.head
            this.head = node
            this.size++
        }
    }

    // Big-O : O(1)t
    addEnd(data){

        if(!this.head){
            this.addStart(data)
        }else{
            const node = new Box(data)

            this.tail.next = node
            this.tail = node
            this.size++
        }
    }

    // Big-O : O(n)t
    addIndex(data, index){
        if(index < 0 || index > this.size) {
            console.log("invalid index");
            return
        }
        if(index === 0) {
            this.addStart(data)
        }else if(index === this.size){
            this.addEnd(data)
        }else{

            const node = new Box(data)

            let curr = this.head
            let count = 0;
            while(count<index-1){
                curr = curr.next
                count++
            }
            node.next = curr.next
            curr.next = node
            this.size++
        }
    }

    // Big-O : O(1)t
    rmStart(){
        if(!this.head) return
        let rm = this.head
        this.head = rm.next
        this.size--
        return rm
    }

    // Big-O : O(n)t
    rmEnd(){
        if(!this.tail) return
        let curr = this.head
        let rm = curr.next
        while(rm.next){
            rm = rm.next
            curr = curr.next
        }
       this.tail = curr
       this.tail.next = null
       this.size--
       return rm.data
    }

    rmIndex(index){
        if(index < 0 || index > this.size-1) return 
        if(index === 0){ 
            return this.rmStart()
        }
        let curr = this.head
        let count = 0
        while(count < index-1){
            curr = curr.next
            count++
        }
        let rm = curr.next
        curr.next = rm.next
        this.size--
        return rm.data
    }

    getIndex(index){
        if(index < 0 || index > this.size-1){
            console.log("Index out of range");
            return
        }else{
            let curr = this.head
            let count = 0
            while(count < index){
                curr = curr.next
                count++
            }
        return curr.data
        }
    }

    getAtData(val){
        let curr = this.head
        let index = 0;
        while (curr) {

            if(curr.data === val){
                return index
            }
            curr = curr.next
            index++
        }
        console.log("No match");
    }

    setAtindex(data, index){
        if(index < 0 || index > this.size-1){
            console.log("Index out of range")
            return false
        }
        let curr = this.head
        for(let i=0; i<index; i++){
            curr = curr.next
        }
        curr.data = data
    }

    reverse(){
        let curr = this.head
        let prevNode = null
        let nextNode =null
        this.tail = this.head
        while(curr){
            nextNode = curr.next
            curr.next = prevNode
            prevNode = curr
            curr = nextNode
        }
        this.head = prevNode
    }

    print(){
        if (!this.head) {
            console.log("Empty list")
            return
        }
        let curr = this.head

        while (curr) {
            console.log(curr.data)
            curr = curr.next
        }
        console.log(`List size : ${this.size}`)
    }

    clearlist(){
        this.head = null
        this.tail = null
        this.size = 0
    }


}

const ll = new Shelf()

ll.addStart(30)
ll.addStart(40)
ll.addStart(50)

ll.addEnd(10)

// ll.addIndex(55,3)
// ll.rmStart();

// console.log(ll.rmEnd());
// console.log(" ");

// ll.print();

// ll.rmIndex(3)
// ll.clearlist()

    // console.log(ll.getIndex(1));

    // const result = ll.getAtData(10)
    // if(result) console.log(result);



    // ll.setAtindex(300,1)


ll.reverse()
ll.print()