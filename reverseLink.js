class Node{
    constructor(data){
        this.data = data
        this.next = null
    }
}

class MainList{
    constructor(){
        this.head = null
        this.tail = null
        this.length = 0
    }

    add(data){
        const node = new Node(data)
        if(!this.head){
            this.head = node
            this.tail = node
            this.length++
        }else{
            this.tail.next = node
            this.tail = node
            this.length++
        }
    }

    reverse(){
        let curr = this.head
        let left = null
        let right = null
        this.tail = this.head

        while(curr){
            right = curr.next
            curr.next = left
            left = curr
            curr = right
        }
        this.head = left
    }

    display(){

        if(!this.head) return false

        let curr = this.head
        
        while(curr){
            console.log(curr.data);
            curr = curr.next
        }
    }



}

const list = new MainList()

list.add(10)
list.add(30)
list.add(50)
list.add(70)
list.add(90)

// console.log(list);

list.reverse()
list.display()
