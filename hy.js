class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
    this.size = 0;
  }

  addNode(data) {
    const node = new Node(data);
    if (!this.head) {
      this.head = node;
      this.tail = node;
    } else {
      this.tail.next = node;
      this.tail = node;
    }
    this.size++;
  }

  addArr(arr) {
    for (let i = 0; i < arr.length; i++) {
      this.addNode(arr[i]);
    }
  }

  binaryToDecimal() {
    let curr = this.head;
    let result = 0;
    while (curr) {
      result = result * 2 + curr.data;
      curr = curr.next;
    }
    return result;
  }

  toDecRec(curr = this.head, result = 0) {
    if (!curr) return result;
    result = result * 2 + curr.data;
    return this.toDecRec(curr.next, result);
  }

  reverse() {
    let curr = this.head;
    let nn = null;
    let pn = null;
    this.tail = this.head;
    while (curr) {
      nn = curr.next;
      curr.next = pn;
      pn = curr;
      curr = nn;
    }
    this.head = pn;
  }

  display() {
    if (!this.head) return console.log("List empty");
    let curr = this.head;
    let print = "";
    for (let i = 0; i < this.size; i++) {
      print += curr.data + "\t";
      curr = curr.next;
    }
    console.log(print);
  }
}

const obj = new LinkedList();

const array = [1, 1, 0, 0, 0];

obj.addArr(array);
// obj.display()
// obj.reverse()
// obj.display()
// console.log(obj);
// console.log(obj.toDecRec());
let res = obj.head;

console.log(res);
