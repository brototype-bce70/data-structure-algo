
const arr = []

//----------Add element to an array------------//
function add(data){
        if(arr.length === 0){
            arr[0] = data
        }else{
            arr[arr.length] = data
        }
    }

    
//---------Search Element and return index position----------//
function search(value){
    for(let i=0;i<arr.length;i++){
        if(arr[i] === value){
            return i
        }
    }
    return console.log("Entered value not available in this array");
}

//-----------Add a element in given index-----------//
function addIndex(index, data){
    if(index < 0 || index > arr.length) return console.log("index not valid");
    if(index === arr.length) return add(data)

    for(var i=arr.length-1,j=arr.length;i>=index;i--,j--){
    
        if(i===index){
            arr[j] = arr[i]
            arr[i] = data
            return
        }
        arr[j] = arr[i]
    }
}

//----------Delete  an element using index------------//
function del(index){
    if(index === undefined) return
    if(index < 0 || index > arr.length-1) return
    if(index === arr.length-1) return arr.length--

    for(let i=0;i<arr.length-1;i++){
        if(i>=index){
            arr[i] = arr[i+1]
        }
    }
    arr.length--
}

//------display array elements---------//
function display(){
    for(let i=0;i<arr.length;i++){
        console.log(arr[i]);
    }
}

    add(10)
    add(20)
    add(30)
    add(40)
    add(50)


    
    
    display()
    // addIndex(3,888)
    // console.log(arr.length);
    // console.log(arr);
    // del(3)
    // console.log(arr);
    
    
    
    // console.log("index at : "+search(40))

    

