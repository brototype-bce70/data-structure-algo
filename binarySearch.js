
const array = [1,2,3,4,5,6,7,8,9]

//---Search a element and return its index pos---//
function binSear(arr, val){
    let start = 0
    let end =arr.length-1

    while(start <= end){
       let middle = Math.floor((start+end)/2)
       if(arr[middle]===val){
        return middle
       }
       if(val < arr[middle]){
        end = middle-1
       }else{
        start = middle+1
       }
    }

    return "Value doesn't match"
}

//---print less than equal to a x value---//

function printSmall(arr, num){
    let start = 0
    let end = arr.length-1

    while(start<=end){
        let mid = Math.floor((start+end)/2)
        if(num === arr[mid]){
            var temp = 0
            while(temp<=mid){
                console.log(arr[temp]);
                temp++
            }
            return
        }
        if(num < arr[mid]){
            end = mid-1
        }else{
            start = mid+1
        }
    }
    return
}

const arr = [80,70,60,50,40,30,20,10]

//---return index position of a decending array---//

function decSear(arr, tar){
    let first = 0
    let last = arr.length-1

    while(first<=last){
        let mid = Math.floor((first+last)/2)
        if(tar === arr[mid]){
            return mid
        }
        if(tar<arr[mid]){
            first = mid+1
        }else{
            last = mid-1
        }
    }
    return "target not found"
}


console.log(decSear(arr,200));


// printSmall(array, 3);



// console.log(binSear(array, 5));

