
class Node{
    constructor(value){
        this.value = value;
        this.next = null;     
    }

}

class sLinkedList{
    constructor(){
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    push(val){
        const newNode = new Node(val);
        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        }else{
            this.tail.next = newNode;
            this.tail = newNode;
        }
        this.length++
        return this
    }

    pop(){
        if (!this.head) return undefined;
        const current = this.head;
        const newTail = current;
        while (current.next) {
            newTail = current;
            current = current.next;
        }
        this.tail = newTail;
        this.tail.next = null;
        this.length--;
        if (this.length === 0) {
            this.head = null;
            this.tail = null;
        }
        return current;
    }

    shift(){
        if(!this.head) return undefined;
        const temp = this.head;
        this.head = temp.next;
        this.length--;
        if(this.length === 0){
            this.tail = null;
        }
        return temp;
    }

    unshift(val){
        const newNode = new Node(val);
        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        }
        newNode.next = this.head;
        this.head = newNode;
        this.length++;
    }

    get(index){
        if(index < 0 || index >this.length) return null;
        var count = 0;
        var current = this.head;
        while (count !== index) {
            current = current.next
            count++
        }
        return current
    }

    set(index, val){
        var foundNode = this.get(index);
        if(foundNode){
            foundNode.value = val;
        }
    }

    insert(index, val){
        if(index < 0 || index > this.length) return false;
        if(index === this.length || index === 0) return this.push(val);

        const newNode = new Node(val);
        var count = 0;
        var current = this.head;
        while(count !== index-1){
            current = current.next;
            count++;
        }
        var prev = current;
        var temp = prev.next
        prev.next = newNode
        newNode.next = temp
        this.length++
        return true
    }

    remove(index){
        if(index < 0 || index >this.length) return false;
        if(index === 0) return this.shift();
        if(index === this.length-1) return this.pop();


        var count = 0;
        var current = this.head;
        while(count !== index-1){
            current = current.next;
            count++;
        }
        var temp = current.next;
        current.next = temp.next;
        this.length--;
        return temp;
    }

    display(){
        var current = this.head;
        var data =null;
        while(current !== null){
            data = data+current.value+ " ";
            current = current.next;
        }
        return data;
    }

    reverse(){
        let current = this.head;
        let prev = null;
        let next = null;

        this.tail = this.head
        while(current){
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        this.head = prev;
    }
}

const sList = new sLinkedList();

sList.push(10);
sList.push(20);
sList.push(30);
// sList.insert(1, 50);


// const value = sList.get(1);

// sList.set(1, 100);

// console.log(sList.remove(1));
// console.log(sList);
sList.reverse();

console.log(sList);
