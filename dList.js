
class Node{
    constructor(data){
        this.data = data
        this.next = null
        this.prev = null
    }
}

class LinkedList{
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }

    addNode(data){
        const node = new Node(data)

        if(!this.head){
            this.head = node
            this.tail = node
        }else{
            this.tail.next = node
            node.prev = this.tail
            this.tail = node
        }
        this.size++
    }

    // add a node on the right side of an given data, so first check this data is in this list

    addRight(val,data){
        const node = new Node(data)

        let curr = this.head
        while(curr){
            if(curr.data === val){
                let temp = curr.next
                node.next = temp
                temp.prev = node
                curr.next =node
                node.prev = curr
                this.size++
                return
            }
            curr = curr.next
        }
        console.log("given data not find in this list");
    }

    display(){
        if(!this.head) return false
        let curr = this.head

        while(curr){
            console.log(curr.data)
            curr = curr.next
        }
    }

    displayReverse(){
        if(!this.tail) return false
        let curr = this.tail

        while(curr){
            console.log(curr.data)
            curr = curr.prev
        }
    }
}

const list = new LinkedList()

list.addNode(10)
list.addNode(20)
list.addNode(30)
list.addNode(40)
list.addNode(50)

list.addRight(10,45)

list.display()

console.log("\n");

list.displayReverse()