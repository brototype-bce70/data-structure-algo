//---------search with element and return index pos------//
const array = [10,8,16,25,60,12,1,4]

function linearSearch(arr, t){
    for(let i=0;i<arr.length;i++){
        if(arr[i]===t){
            return i
        }
    }
    return false
}

//-----getting values of less than or equal x values in an array-----//

function findValue(arr, val){
    let res = []
    for(let i=0,j=0;i<arr.length;i++){
        if(arr[i] <= val){
            res[j] = arr[i]
            j++
        }
    }
    if(!res.length){
        return "invalid"
    }else{
        return res
    }
}

//-----find odd numbers in an array------//

function findOdd(arr){
    let res = []
    for(let i=0,j=0;i<arr.length;i++){
        if(arr[i]%2 != 0){
            res[j] = arr[i]
            j++
        }
    }
    if(!res.length){
        return "no odd numbers in this array"
    }else{
        return res
    }
}


console.log(findOdd(array));

// console.log(findValue(array, 0));

// console.log(linearSearch(array, 10)); //0
// console.log(linearSearch(array, 600)); //4
