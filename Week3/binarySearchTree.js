class Node {
  constructor(data) {
    this.data = data;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  isEmpty() {
    return this.root === null;
  }

  insert(data) {
    const newNode = new Node(data);
    if (this.isEmpty()) {
      this.root = newNode;
    } else {
      this.insertNode(this.root, newNode);
    }
  }

  insertNode(root, newNode) {
    if (newNode.data < root.data) {
      if (root.left === null) {
        root.left = newNode;
      } else {
        this.insertNode(root.left, newNode);
      }
    } else {
      if (root.right === null) {
        root.right = newNode;
      } else {
        this.insertNode(root.right, newNode);
      }
    }
  }

  search(data) {
    return this.searchHelper(this.root, data);
  }

  searchHelper(root, data) {
    if (!root) return false;
    if (root.data === data) {
      return true;
    } else if (data < root.data) {
      return this.searchHelper(root.left, data);
    } else {
      return this.searchHelper(root.right, data);
    }
  }

  preorder() {
    this.preorderHelper(this.root);
  }

  preorderHelper(root) {
    if (root) {
      console.log(root.data);
      this.preorderHelper(root.left);
      this.preorderHelper(root.right);
    }
  }

  inorder() {
    this.inorderHelper(this.root);
  }

  inorderHelper(root) {
    if (root) {
      this.inorderHelper(root.left);
      console.log(root.data);
      this.inorderHelper(root.right);
    }
  }

  postorder() {
    this.postorderHelper(this.root);
  }

  postorderHelper(root) {
    if (root) {
      this.postorderHelper(root.left);
      this.postorderHelper(root.right);
      console.log(root.data);
    }
  }

  levelOrder() {
    const queue = [];
    queue.push(this.root);

    while (queue.length) {
      let curr = queue.shift();
      console.log(curr.data);
      if (curr.left) {
        queue.push(curr.left);
      }
      if (curr.right) {
        queue.push(curr.right);
      }
    }
  }

  findMin() {
    return this.findMinHelper(this.root);
  }

  findMinHelper(root) {
    if (!root.left) {
      return root.data;
    } else {
      return this.findMinHelper(root.left);
    }
  }

  findMax() {
    return this.findMaxHelper(this.root);
  }

  findMaxHelper(root) {
    if (!root.right) {
      return root.data;
    } else {
      return this.findMaxHelper(root.right);
    }
  }

  deleteVal(data) {
    this.root = this.deleteHelper(this.root, data);
  }

  deleteHelper(root, data) {
    if (root === null) {
      return root;
    }
    if (data < root.data) {
      root.left = this.deleteHelper(root.left, data);
    } else if (data > root.data) {
      root.right = this.deleteHelper(root.right, data);
    } else {
      if (!root.left && !root.right) {
        return null;
      } else if (!root.left) {
        return root.right;
      } else if (!root.right) {
        return root.left;
      }

      root.data = this.findMinHelper(root.right);
      root.right = this.deleteHelper(root.right, root.data);
    }
    return root;
  }
}

const bst = new BinarySearchTree();

bst.insert(10);
bst.insert(5);
bst.insert(15);
bst.insert(3);

bst.preorder();
// bst.insert(7)
// bst.insert(13)
// bst.insert(18)

// console.log(bst.search(10));
// console.log(bst.search(5));
// console.log(bst.search(15));

// bst.postorder()

// bst.levelOrder()

// console.log(bst.findMax())
// console.log(bst.findMin())

// bst.deleteVal(10)
// console.log("___________");
// bst.levelOrder()
