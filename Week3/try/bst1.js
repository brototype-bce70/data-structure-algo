class Node {
    constructor(data){
        this.data = data
        this.left = null
        this.right = null
    }
}

class BinarySearchTree {
    constructor(){
        this.root = null
    }

    insert(data){
        const newNode = new Node(data)
        if(!this.root){
            this.root = newNode
        }else{
            this._insert(this.root , newNode)
        }
    }

    _insert(root , node){
        if(node.data < root.data){
            if(!root.left){
                root.left = node
            }else{
                this._insert(root.left , node)
            }
        }else if(node.data > root.data){
            if(!root.right){
                root.right = node
            }else{
                this._insert(root.right , node)
            }
        }
    }

    contains(data){
        let root = this.root

        while(root){
            if(data === root.data) return true
            if(data < root.data){
                root = root.left
            }else if(data > root.data){
                root = root.right
            }
        }
        return false
    }

    delete(data){
        this.root = this._delete(this.root , data)
    }

    _delete(root , data){
        if(!root) return root

        if(data < root.data){
            root.left = this._delete(root.left , data)
        }else if(data > root.data){
            root.right = this._delete(root.right , data)
        }else{
            if(!root.left && !root.right){
                return null
            }else if(!root.left){
                return root.right
            }else if(!root.right){
                return root.left
            }

            root.data = this.findMin(root.right)
            root.right = this._delete(root.right , root.data)
        }
        return root
    }

    findMin(root){
        let min = root.data

        while(root.left){
            min = root.left.data
            root = root.left
        }
        return min
    }

    preorder(){
        this._preorder(this.root)
    }

    _preorder(root){
        if(root){
            console.log(root.data);
            this._preorder(root.left)
            this._preorder(root.right)
        }
    }

    inorder(){
        this._inorder(this.root)
    }

    _inorder(root){
        if(root) {
            this._inorder(root.left)
            console.log(root.data);
            this._inorder(root.right)
        }
    }

    postorder(){
        this._postorder(this.root)
    }

    _postorder(root){
        if(root){
            this._postorder(root.left)
            this._postorder(root.right)
            console.log(root.data);
        }
    }

    findClose(data){
        let root = this.root
        let closest = root.data

        while(root){
            if(Math.abs(root.data - data) < Math.abs(closest - data)) {
                closest = root.data
            }
            if(root.data < data){
                root = root.right
            }else if(root.data > data){
                root = root.left
            }else{
                return root.data
            }
        }
        return closest
    }

    isBST(){
        const root = this.root
        return this._isBST(root, Number.NEGATIVE_INFINITY , Number.POSITIVE_INFINITY)
    }

    _isBST(root , min , max){
        if(!root) return true

        if(root.data <= min || root.data >= max){
            return false
        }
        return this._isBST(root.left , min , root.data) && this._isBST(root.right , root.data , max)
    }
}

const bst = new BinarySearchTree()

bst.insert(40)
bst.insert(30)
bst.insert(50)
bst.insert(20)

// console.log(bst.findClose(28));
console.log(bst.isBST());















// bst.delete(20)
// bst.delete(30)
// bst.delete(40)


// console.log(bst.contains(40));
// console.log(bst.contains(30));
// console.log(bst.contains(10));
// console.log(bst.contains(20));








// console.log("-------------");
// bst.inorder()
// console.log("-------------");
// bst.postorder()
// // console.log(bst);


