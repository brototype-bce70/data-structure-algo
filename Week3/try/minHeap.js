class MinHeap {
    constructor(){
        this.array = []
    }

    //---return left child index
    leftChildIdx(index){
        return (index * 2)+1
    }
    //---return right child index
    rightChildIdx(index){
        return (index * 2)+2
    }
    //---return parent index
    parentIdx(index){
        return Math.floor((index - 1) / 2)
    }

    //---Node have leaf return false
    isLeaf(index){
        return(
            index >= Math.floor(this.array.length / 2) && index < this.array.length-1
        )
    }

}