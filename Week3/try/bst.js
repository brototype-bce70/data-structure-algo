class Node {
    constructor(data){
        this.data = data
        this.left = null
        this.right = null
    }
}

class BinarySearchTree {
    constructor(){
        this.root = null
    }

    insert(data){
        const newNode = new Node(data)

        if(!this.root){
            this.root = newNode
        }else{
            this._insert(this.root, newNode)
        }
    }

    _insert(root, newNode){
        if(newNode.data < root.data){
            if(!root.left){
                root.left = newNode
            }else{
                this._insert(root.left, newNode)
            }
        }else if(newNode.data > root.data){
            if(!root.right){
                root.right = newNode
            }else{
                this._insert(root.right, newNode)
            }
        }else{
            return
        }
    }

    search(data){
        if(!this.root) return false
        let curr = this.root

        while(curr){
            if(curr.data === data) return true
            if(data < curr.data){
                curr = curr.left
            }else if(data > curr.data){
                curr = curr.right
            }
        }

        return false
    }

    preorder(){
        if(!this.root) return
        this._preorder(this.root)
    }

    _preorder(root){
        if(root){
            console.log(root.data);
            this._preorder(root.left)
            this._preorder(root.right)
        }
    }

    inorder(){
        if(!this.root) return
        this._inorder(this.root)
    }

    _inorder(root){
        if(root){
            this._inorder(root.left)
            console.log(root.data);
            this._inorder(root.right)
        }
    }

    postorder(){
        if(!this.root) return
        this._postorder(this.root)
    }

    _postorder(root){
        if(root){
            this._postorder(root.left)
            this._postorder(root.right)
            console.log(root.data);
        }
    }
    
    bfs(){
        if (!this.root) return false
        let queue = []
        queue.push(this.root)
        let output = []

        while(queue.length){
            const node = queue.shift()
            if(node.left){
                queue.push(node.left)
            }
            if(node.right){
                queue.push(node.right)
            }
            output.push(node.data)
        }
        return output
    }

    remove(data){
        this.root = this._remove(this.root , data)
    }

    _remove(root, data){
        if(!root) return root

        if(data < root.data){
            this._remove(root.left , data)
        }else if(data > root.data){
            this._remove(root.right , data)
        }else{
            if(!root.left && !root.right){
                return null
            }else if(!root.left){
                return root.right
            }else if(!root.right){
                return root.left
            }

            root.data = this.minVal(root.right)

            root.right = this._remove(root.right , root.data)
        }
        return root
    }

    minVal(root){
        let min = root.data

        while(root.left){
            min = root.left.data
            root = root.left
        }
        return min
    }

    isBst(){
        const root = this.root
        return this._isBst(root , Number.NEGATIVE_INFINITY , Number.POSITIVE_INFINITY)
    }

    _isBst(root , min , max){
        if(!root) return true

        if(root.data <= min || root.data >= max){
            return false
        }
        return this._isBst(root.left , min , root.data) && this._isBst(root.right , root.data , max)
    }

    findClosest(target){
        let root = this.root
        let closest = root.data

        while(root){
            if(Math.abs(root.data - target) < Math.abs(closest - target)){
                closest = root.data
            }
            if(root.data < target){
                root = root.right
            }else if(root.data > target){
                root = root.left
            }else{
                return root.data
            }
        }
        return closest
    }
}



const bst = new BinarySearchTree()

bst.insert(6)
bst.insert(4)
bst.insert(9)
bst.insert(2)
bst.insert(5)
bst.insert(8)
bst.insert(15)
bst.remove(4)

// console.log(bst.search(100));
// console.log(bst.search(5));
// console.log(bst.search(15));

// bst.postorder()
console.log(bst.findClosest(1))