class Graph {
    constructor(){
        this.list = {}
    }

    addVertex(vertex){
        if(!this.list[vertex]){
            this.list[vertex] = new Set()
        }
    }

    addEdge(vertex1 , vertex2){
        if(!this.list[vertex1]) this.addVertex(vertex1)
        if(!this.list[vertex2]) this.addVertex(vertex2)

        this.list[vertex1].add(vertex2)
        this.list[vertex2].add(vertex1)
    }

    removeEdge(vertex1 , vertex2){
        this.list[vertex1].delete(vertex2)
        this.list[vertex2].delete(vertex1)
    }

    removeVertex(vertex){
        if(!this.list[vertex]) return

        for(let edge of this.list[vertex]){
            this.removeEdge(vertex , edge)
        }
        delete this.list[vertex]
    }

    hasEdge(vertex1 , vertex2){
        return (
            this.list[vertex1].has(vertex2) &&
            this.list[vertex2].has(vertex1)
        )
    }

    bfs(vertex){
        let queue = [vertex]
        let res = []
        let visited = new Set()

        visited.add(vertex)

        while(queue.length){
            let curr = queue.shift()
            res.push(curr)

            this.list[curr].forEach(adjacentVertex => {
                
                if(!visited.has(adjacentVertex)){
                    visited.add(adjacentVertex)
                    queue.push(adjacentVertex)
                }
            });
        }
        return res
    }

    dfs(vertex){
        let stack = [vertex]
        let res = []
        let visited = new Set()

        while(stack.length){
            let curr = stack.pop()
            if(!visited.has(curr)){
                visited.add(curr)
                res.push(curr)

                this.list[curr].forEach(adjacentVertex => {
                    stack.push(adjacentVertex)
                })
            }
        }
        return res
    }    

    display(){
        for(let vertex in this.list){
            console.log(vertex + "->" + [...this.list[vertex]]);
        }
    }
}

const gph = new Graph()

gph.addVertex('A')
gph.addVertex('B')
gph.addVertex('C')

gph.addEdge('A' , 'B')
gph.addEdge('B' , 'C')
gph.addEdge('B' , 'E')
gph.addEdge('B' , 'Z')


console.log(gph.dfs('A'))







// gph.removeVertex('B')
// console.log(gph.dfs('A'))

// gph.display()