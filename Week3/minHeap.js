class MinHeap {
    constructor(){
        this.heap = []
    }

    leftIdx(index){
        return (index * 2) + 1
    }

    rightIdx(index){
        return (index * 2) + 2
    }

    parentIdx(index){
        return Math.floor((index - 1) / 2)
    }

    //---there is no leaf at this index retun true---//
    isLeaf(index){
        return (
            index >= Math.floor(this.heap.length / 2) && index < this.heap.length-1
        )
    }

    swap(index1 , index2){
        [this.heap[index1] , this.heap[index2]] = [this.heap[index2] , this.heap[index1]]
    }

    add(data){
        this.heap.push(data)
        this.heapifyUp(this.heap.length-1)
    }

    heapifyUp(index){
        let currIndex = index ,
            parentIndex = this.parentIdx(index)
            
            while(currIndex > 0 && this.heap[currIndex] < this.heap[parentIndex]){
                this.swap(currIndex , parentIndex)
                currIndex = parentIndex
                parentIndex = this.parentIdx(parentIndex)
            }
    }

    removeMin(){
        if(this.heap.length < 1) return 'Heap is empty'

        const min = this.heap[0]
        const end = this.heap.pop()

        this.heap[0] = end
        this.heapifyDown(0)
        return min
    }

    heapifyDown(index){
        if(this.isLeaf(index) === false){
            let leftChildIdx = this.leftIdx(index)
            let rightChildIdx = this.rightIdx(index)
            let minIdx = index

            if(this.heap[leftChildIdx] < this.heap[minIdx]){
                minIdx = leftChildIdx
            }
            if(this.heap[rightChildIdx] < this.heap[minIdx]){
                minIdx = rightChildIdx
            }

            if(minIdx !== index){
                this.swap(index , minIdx)
                this.heapifyDown(minIdx)
            }
        }
    }

    buildHeap(arr){
        this.heap = arr

        for(let i=Math.floor(this.heap.length / 2) ; i >= 0 ; i--){
            this.heapifyDown(i)
        }
    }

    peek(){
        return this.heap[0]
    }

    print(){
        let i=0

        while(this.isLeaf(i) === false){
            console.log('Parent :' + this.heap[i]);
            console.log('Left Child :' + this.heap[this.leftIdx(i)]);
            console.log('Right Child :' + this.heap[this.rightIdx(i)]);
            i++
        }
    }
}

const obj = new MinHeap()

// obj.add(20)
// obj.add(10)
// obj.add(30)

obj.buildHeap([20,10,30,25,50,40,60])

obj.print()