class Node {
    constructor(data){
        this.data = data
        this.left = null
        this.right = null
    }
}

class BST {
    constructor(){
        this.root = null
    }

    isEmpty() {
        return this.root === null
    }

    

    insert(data){
        const newNode = new Node(data)
        if(!currNode){
            this.root = newNode
            return
        }
        let currNode = this.root

        while(true){
            if(data < currNode.data){
                if(!currNode.left){
                    currNode.left = newNode
                    break;
                }else{
                    currNode = currNode.left
                }
            }else{
                if(!currNode.right){
                    currNode.right = newNode
                    break;
                }else{
                    currNode = currNode.right
                }
            }
        }
    }


    contains(data){
        let currNode = this.root

        while(currNode !== null){
            if(data < currNode.data){
                currNode = currNode.left
            }else if(data > currNode.data){
                currNode = currNode.right
            }else{
                return true
            }
        }
        return false
    }

    preorder(){
        this.preorderHelper(this.root)
    }

    preorderHelper(node){
        if(node !== null){
            console.log(node.data);
            this.preorderHelper(node.left)
            this.preorderHelper(node.right)
        }
    }

}

const bst = new BST()

bst.insert(10)
bst.insert(8)
bst.insert(11)
bst.insert(4)
bst.insert(9)

// console.log(bst.contains(11));

// console.log(bst.preorder())
bst.preorder()