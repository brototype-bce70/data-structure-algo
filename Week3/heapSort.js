
function heapify(arr , length , parent){

    let leftChildIdx = (parent * 2) + 1
    let rightChildIdx = (parent * 2) + 2
    let largestIdx = parent

    if(leftChildIdx < length && arr[leftChildIdx] > arr[largestIdx]){
        largestIdx = leftChildIdx
    }
    if(rightChildIdx < length && arr[rightChildIdx] > arr[largestIdx]){
        largestIdx = rightChildIdx
    }

    if(largestIdx !== parent){
        [arr[largestIdx] , arr[parent]] = [arr[parent] , arr[largestIdx]]
        heapify(arr ,length , largestIdx)
    }
}

function heapSort(arr){


    for(let i = Math.floor((arr.length / 2) - 1) ; i >= 0 ; i--){
        heapify(arr, arr.length, i)
    }

    for(let i = arr.length -1 ; i > 0 ; i--){
        [arr[0] , arr[i]] = [arr[i] , arr[0]]
        heapify(arr, i , 0)
    }
    return arr
}

console.log(heapSort([4,2,7,5,1,3]))