class MaxHeap {
    constructor(){
        this.heap = []
    }

    //---getting index of parent---//
    parentInd(index){
        return Math.floor((index - 1) / 2)
    }

    //---getting index of left child---//
    leftChildInd(index){
        return (2*index) + 1
    }

    //---getting index of right child---//
    rightChildInd(index){
        return (2*index) + 2
    }

    isLeaf(index){
        return (
            index >= Math.floor(this.heap.length / 2) && index <= this.heap.length-1
        )
    }

    //---swap elements using ES6 destructuring---//
    swap(index1 , index2){
        [this.heap[index1] , this.heap[index2]] = [this.heap[index2] , this.heap[index1]]
    }

    add(data){
        this.heap.push(data)
        this.heapifyUp(this.heap.length-1)
    }

    heapifyUp(index){
        let currIdx = index
        let parentIdx = this.parentInd(currIdx)

        while(currIdx > 0 && this.heap[currIdx] > this.heap[parentIdx]){
            this.swap(currIdx , parentIdx)
            currIdx = parentIdx
            parentIdx = this.parentInd(parentIdx)
        }
    }

    removeMax(){
        if(this.heap.length < 1) return 'heap is Empty'

        const max = this.heap[0]
        const end = this.heap.pop()

        this.heap[0] = end
        this.heapifyDown(0)

        return max
    }

    heapifyDown(index){
        if(!this.isLeaf(index)){
            let leftChildIdx = this.leftChildInd(index)
            let rightChildIdx = this.rightChildInd(index)
            let largestIdx = index

            if(this.heap[leftChildIdx] > this.heap[largestIdx]){
                largestIdx = leftChildIdx
            }

            if(this.heap[rightChildIdx] >= this.heap[largestIdx]){
                largestIdx = rightChildIdx
            }

            if(largestIdx !== index){
                this.swap(index , largestIdx)
                this.heapifyDown(largestIdx)
            }
        }
    }

    buildHeap(arr){
        this.heap = arr

        for(let i = Math.floor(this.heap.length / 2); i>= 0 ; i--){
            this.heapifyDown(i)
        }
    }


    peek(){
        return this.heap[0]
    }

    print(){
        let i=0

        while(!this.isLeaf(i)){
            console.log("Parent :" , this.heap[i]);
            console.log("left Child :" , this.heap[this.leftChildInd(i)]);
            console.log("Right Child :" , this.heap[this.rightChildInd(i)]);
            i++
        }
    }

}

const obj = new MaxHeap()

obj.add(30)
obj.add(20)
obj.add(10)
obj.add(50)
obj.add(15)
obj.add(8)
obj.add(16)
obj.add(12)

obj.print()