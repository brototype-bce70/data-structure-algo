///---Adjacency Matrix

const matrix = [
    [0,1,0],
    [1,0,1],
    [0,1,0]
]

///---Adjacency List

const list = {
    'A' : ['B'],
    'B' : ['A','C'],
    'C' : ['B']
}


class Graph {
    constructor(){
        this.adjacencyList = {}
    }

    addVertex(vertex){
        if(!this.adjacencyList[vertex]){
            this.adjacencyList[vertex] = new Set()
        }
    }

    addEdge(vertex1 , vertex2){
        if(!this.adjacencyList[vertex1]){
            this.addVertex(vertex1)
        }
        if(!this.adjacencyList[vertex2]){
            this.addVertex(vertex2)
        }

        this.adjacencyList[vertex1].add(vertex2)
        this.adjacencyList[vertex2].add(vertex1)
    }

    removeEdge(vertex1 , vertex2){
        this.adjacencyList[vertex1].delete(vertex2)
        this.adjacencyList[vertex2].delete(vertex1)
    }

    removeVertex(vertex){
        if(!this.adjacencyList[vertex]) return

        for(let adjacencyVertex of this.adjacencyList[vertex]){
            this.removeEdge(vertex , adjacencyVertex)
        }
        delete this.adjacencyList[vertex]
    }

    hasEdge(vertex1 , vertex2){
        return (
            this.adjacencyList[vertex1].has(vertex2) &&
            this.adjacencyList[vertex2].has(vertex1)
        )
    }

    bfs(vertex){
        let queue = [vertex]
        let res = []
        let visited = new Set()

        visited.add(vertex)

        while(queue.length){
            let curr = queue.shift()
            res.push(curr)

            this.adjacencyList[curr].forEach(adjacentVertex => {
                if(!visited.has(adjacentVertex)){
                    visited.add(adjacentVertex)
                    queue.push(adjacentVertex)
                }
            });
        }
        return res
    }

    dfs(vertex){
        let stack = [vertex]
        let res = []
        let visited = new Set()

        while(stack.length){
            let curr = stack.pop()
            if(!visited.has(curr)){
                visited.add(curr)
                res.push(curr)

                this.adjacencyList[curr].forEach(adjVertex => {
                    stack.push(adjVertex)
                })
            }
        }
        return res
    }

    display(){
        for(let vertex in this.adjacencyList){
            console.log(vertex + '->' + [...this.adjacencyList[vertex]]);
        }
    }
}

const graph = new Graph()

graph.addVertex('A')
graph.addVertex('B')
graph.addVertex('C')

graph.addEdge('A' , 'B')
graph.addEdge('B' , 'C')
graph.addEdge('A' , 'D')
// graph.removeEdge('A' , 'B')
// graph.removeVertex('B')

console.log(graph.dfs('A'));
// graph.display()

// console.log(graph.hasEdge('A','C'));