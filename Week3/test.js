


class Graph {
    constructor(){
        this.graph = {}
    }

    addVertex(vertex){
      if(!this.graph[vertex]){
        this.graph[vertex] = new Set()
      }
    }

    addEdge(vertex1 , vertex2){
      if(!this.graph[vertex1]) this.addVertex(vertex1)
      if(!this.graph[vertex2]) this.addVertex(vertex2)

        this.graph[vertex1].add(vertex2)
        this.graph[vertex2].add(vertex1)
    }

    bfs(vertex){
        let queue = [vertex]
        let res = []
        let visited = new Set()

        visited.add(vertex)

        while(queue.length){
            let curr = queue.shift()
            res.push(curr)

            this.graph[vertex].forEach(adjacentVertex => {
                if(!visited.has(adjacentVertex)){
                    visited.add(adjacentVertex)
                    queue.push(adjacentVertex)
                }
            });
        }
        return res
    }


}

const gph = new Graph()

gph.addVertex('A')

gph.addEdge('A' , 'B')
gph.addEdge('B' , 'C')

console.log(gph.bfs('A'));
