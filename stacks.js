

 class Node{
    constructor(value){
        this.value = value;
        this.next = null;
    }
 }

 class Stack{
    constructor(){
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    // this add a node to top of the stack
    addNode(val){
        var newNode = new Node(val);
        if (!this.first) {
            this.first = newNode;
            this.last = newNode;
        }else{
            var temp = this.first;
            this.first = newNode;
            this.first.next = temp;
        }
        return ++this.size;
    }

    //this remove a node at the top of the stack and return its value
    removeNode(){
        if(!this.first) return null;
        var temp = this.first;
        if(this.first == this.last){
            this.last = null;
        }
        this.first=this.first.next;
        this.size--;
        return temp.value;
    }

    // searchValue(val){
    //     var temp = this.first
    //     while (temp.value != val || this.last) {
    //         temp = temp.next;
    //     }
    //     if(temp.value == val){
    //         return temp;
    //     }
    // }

 }

 const stack = new Stack;

 stack.addNode(10);
 stack.addNode(20);
 stack.addNode(30);

//  console.log(stack.last);
//  console.log(stack.size);

 stack.addNode(50);

//  console.log(stack.first);

//   var temp = stack.removeNode();

//  console.log(stack.removeNode());

//  console.log(stack.first);

console.log(stack);