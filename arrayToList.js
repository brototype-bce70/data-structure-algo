
class Node{
    constructor(data){
        this.data = data
        this.next = null
    }
}

class List{
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }

    toTail(data){
        const node = new Node(data)

        if(!this.head){
            this.head = node
            this.tail = node
        }else{
            this.tail.next = node
            this.tail = node
        }
        this.size++
    }

    deleteValue(val){
        let curr = this.head
        if(curr.data===val){
            this.head = curr.next
            this.size--
            return curr.data
        }
        let temp = curr.next
        while(temp.data !== val){
            if(temp.next === null){
                return "Value not find in this list"
            }
            curr = curr.next
            temp = temp.next
        }
        curr.next = temp.next
        this.size--
        return temp.data
    }

    //----insert node after x data-----//
    insertAfter(val, data){
        const node = new Node(data)
        let curr = this.head
        let last = this.tail
        if(curr.data === val){
            node.next = curr.next
            curr.next = node
            this.size++
            return
        }else if(last.data === val){
            last.next = node
            this.tail = node
            this.size++
            return
        }else{
            curr = curr.next
            while(curr){
                if(curr.data===val){
                    node.next = curr.next
                    curr.next = node
                    this.size++
                    return
                }
                curr = curr.next
            }
        }
        console.log("Given val didn't find in this list")
    }

    //-------insert node before x data--------//
    insertBefore(val, data){
        const node = new Node(data)

        let curr = this.head
        if(curr.data === val){
            node.next = curr
            this.head = node
        }else{
            let temp = curr.next

            while(temp.data!==val){
                temp = temp.next
                curr = curr.next
                if(!temp){
                    console.log("given data not found in this list");
                    return
                }
            }
            node.next = temp
            curr.next = node
        }
        this.size++
    }

    //----Write a program to remove duplicates in a sorted singly linked list----//
    rmDuplicate() {
        let curr = this.head
        let temp = curr.next

        while(temp){
            if(curr.data === temp.data){
                temp = temp.next
                this.size--
            }else{
                curr.next = temp
                curr = curr.next
                temp = temp.next 
            } 
        }
    }

    convert(arr,count =0){
        if(count===arr.length){
            return
        }
        this.toTail(arr[count])
        count = count+1
        this.convert(arr,count)
    }


    display(){
        if(!this.head) return false

        let curr = this.head
        let res = ""
        while(curr){
            res += curr.data+" "
            curr = curr.next
        }
        console.log(res);
        console.log("List size : "+this.size);
    }
}


const list = new List()

const arr = [10,20,30,30,30,40,40,50,60,60,70]

// for(let i=0;i<arr.length;i++){
//     list.toTail(arr[i])
// }


// console.log("Deleted node : "+list.deleteValue(30)+"\n")
// list.display()

// list.insertBefore(30,555)


// list.display()
// list.rmDuplicate()
// list.display()
// console.log(list);

list.convert(arr)
list.display()
