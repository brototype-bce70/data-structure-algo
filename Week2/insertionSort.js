
function insSort(arr){
    let iSize = arr.length

    for(let i=1; i<iSize; i++){
        let j = i
        while(j>0 && arr[j-1]>arr[j]){
            var temp = arr[j]
            arr[j] = arr[j-1]
            arr[j-1] = temp
            j--
        }
    }
    return arr
}

// const array = [48,52,3,6,1,9,26]

// console.log(insSort(array));

//---Decending Order---//

function decOrder(arr){
    for(let i=1; i<arr.length; i++){
        let j=i
        while(j>0 && arr[j-1]<arr[j]){
            var temp = arr[j]
                arr[j] = arr[j-1]
                arr[j-1] = temp
                j--
        }
    }
    return arr
}                  

// const array = [48,52,3,6,1,9,26]

// console.log(decOrder(array));

//---sort and return odd numbers---//

function rtnOdd(arr){
    let res = ""
    for(let i=1; i<arr.length; i++){
        let j=i
        while(j>0 && arr[j-1]>arr[j]){
            var temp = arr[j]
                arr[j] = arr[j-1]
                arr[j-1] = temp
                j-- 
        }
    }
    for(i=0; i<arr.length; i++){
        if(arr[i]%2 !== 0) res += arr[i] + " "
    }
    return res
}

const array = [48,52,3,6,1,9,26]

console.log(rtnOdd(array));