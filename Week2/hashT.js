class Table {
    constructor(size){
        this.table = new Array(size)
    }

    hash(key){
        let hash = 0
        for(let i=0 ; i<key.length ; i++){
            hash += key.charCodeAt(i)
        }
        return hash % this.table.length
    }

    set(key , val){
        const index = this.hash(key)

        if(this.table[index]){
            for(let i=0 ; i<this.table[index].length ; i++){
                if(this.table[index] [i] [0] === key){
                    this.table[index] [i] [1] = val
                    return
                }
            }
            this.table[index].push([key , val])
        }else{
            this.table[index] = []
            this.table[index].push([key , val])
        }
    }

    get(key){
        const index = this.hash(key)

        if(this.table[index]){
            for(let i=0 ; i<this.table[index].length ; i++){
                if(this.table[index] [i] [0] === key){
                    return this.table[index] [i] [1]
                }
            }
        }
        return undefined
    }

    display(){
       for(let i=0 ; i<this.table.length ; i++){
            if(this.table[i]){
            console.log(this.table[i]);
            }
       }
    }
}

const hh = new Table(10)

hh.set("name" , "Bibi")
hh.set("mane" , "amal")
hh.set("place" , "idukki")

// console.log(hh.get("name"));

hh.display()
