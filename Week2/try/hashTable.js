
class HashTable {
    constructor(size){
        this.table = new Array(size)
    }

    hash(key){
        let hash = 0 
        for(let i=0 ; i<key.length ; i++){
            hash += key.charCodeAt(i)
        }
        return hash % this.table.length
    }

    push(key , val){

        const index = this.hash(key)

        if(this.table[index]){
            for(let i=0 ; i<this.table[index].length ; i++){
                if(this.table[index] [i] [0] === key){
                    this.table[index] [i] [1] = val
                    return
                }
            }
            this.table[index].push([key , val])
        }else{
            this.table[index] = []
            this.table[index].push([key , val])
        }
    }

    pop(key){
        const index = this.hash(key)

        if(this.table[index]){
            for(let i=0 ; i<this.table[index].length ; i++){
                if(this.table[index] [i] [0] === key){
                    return true
                }
            }
        }
        return false
    }

    get(key){
        const index = this.hash(key)
        if(this.table[index]) {
        for(let i=0 ; i<this.table[index].length ; i++){
            if(this.table[index] [i] [0] === key){
                return this.table[index] [i] [1]
            }
        }
    }
    return undefined
    }
}

const hh = new HashTable(10)

hh.push("Name" , "Amal")
hh.push("maNe" , "lets hack")
hh.push("age" , 24)

console.log(hh.get("Name"));
console.log(hh.get("age"));