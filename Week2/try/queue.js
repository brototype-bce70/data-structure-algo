class Node { 
    constructor(data){
        this.data = data
        this.next = null
    }
}

class Queue {
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }

    enqueue(data){
        const node = new Node(data)

        if(!this.head){
            this.head = node
            this.tail = node
        }else{
            this.tail.next = node
            this.tail = node
        }
        this.size++
    }

    dequeue(){
        if(!this.head) return false

        let curr = this.head
        this.head = curr.next
        curr.next = null
        this.size--
        return curr.data
    }

    peek(){
        if(!this.head) return false

        return this.head.data
    }

    display(){
        if(!this.head) return false

        let curr = this.head
        while(curr){
            console.log(curr.data);
            curr = curr.next
        }
    }
}

const queue = new Queue()

queue.enqueue(10)
queue.enqueue(20)
queue.enqueue(30)
queue.enqueue(40)
queue.enqueue(50)

// console.log(queue.peek());
queue.display()