function bubbSort(arr){
    for(let i=0 ; i<arr.length-1 ; i++){
        for(let j=0 ; j<arr.length-i-1 ; j++){
            if(arr[j] > arr[j+1]){
                var temp = arr[j]
                    arr[j] = arr[j+1]
                    arr[j+1] = temp
            }
        }
    }
    return arr
}

const array = [2,9,4,5,7,1,3,6,8]

console.log(bubbSort(array))