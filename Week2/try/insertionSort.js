function insSort(arr){

    for(let i=1 ; i<arr.length ; i++){
        let j = i
        while(j>0 && arr[j-1] > arr[j]){
            var temp = arr[j-1]
                arr[j-1] = arr[j]
                arr[j] = temp
                j--
        }
    }
    return arr
}

const array = [2,9,4,5,7,1,3,6,8]

console.log(insSort(array))