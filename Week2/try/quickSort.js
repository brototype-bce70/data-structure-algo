
function quickSort(arr){
    if(arr.length < 2) return arr

    const pivot = arr[arr.length - 1]
    let leftArr = []
    let rightArr = []

    for(let i=0 ; i<arr.length-1 ; i++){
        if(arr[i] <= pivot){
            leftArr.push(arr[i])
        }else if(arr[i] > pivot){
            rightArr.push(arr[i])
        }
    }
    // console.log(leftArr);
    // console.log(pivot);
    // console.log(rightArr);

    return [...quickSort(leftArr), pivot , ...quickSort(rightArr)]
}

const array = [2,9,4,5,7,1,3,6,8]

console.log(quickSort(array))