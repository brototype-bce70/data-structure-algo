
function mgSort(arr){
    if(arr.length < 2) return arr

    const mid = Math.floor(arr.length / 2)
    let leftArr = arr.slice(0,mid)
    let rightArr = arr.slice(mid)

    return merge(mgSort(leftArr) , mgSort(rightArr))
}

function merge(leftArr , rightArr){
    let sortedArr = []

    while(leftArr.length && rightArr.length){
        if(leftArr[0]<rightArr[0]){
            sortedArr.push(leftArr.shift())
        }else{
            sortedArr.push(rightArr.shift())
        }
    }

    return [...sortedArr, ...leftArr, ...rightArr]
}

const array = [2,9,4,5,7,1,3,6,8]

console.log(mgSort(array))