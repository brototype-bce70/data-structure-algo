
class Node {
    constructor(data){
        this.data = data
        this.next = null
    }
}

class List {
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }

    push(data){
        const node = new Node(data)

        if(!this.head){
            this.head = node
            this.tail = node
        }else{
            node.next = this.head
            this.head = node
        }
        this.size++
    }

    pop(){
        if(!this.head) return false

        let curr = this.head
        this.head = curr.next
        curr.next = null
        this.size--

        return curr.data
    }

    isEmpty(){
        if(!this.head){
            return true
        }
    }

    peek(){
        if(this.isEmpty()) return "Empty"

        return this.head.data
    }

    length(){
        return this.size
    }

    display(){
        if(!this.head) return "Empty"

        let curr = this.head
        let data = ""
        while(curr){
            data += curr.data + " "
            curr = curr.next
        }
        return data
    }
}

const list = new List()



list.push(10)
list.push(20)
list.push(30)
list.push(40)
list.push(50)

console.log(list.display());
console.log(list.pop());
