class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class Queue {
  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }

  enqueue(data) {
    const node = new Node(data);

    if (!this.head) {
      this.head = node;
      this.tail = node;
    } else {
      this.tail.next = node;
      this.tail = node;
    }
    this.length++;
  }

  dequeue() {
    if (!this.head) return "Empty";

    let curr = this.head;
    this.head = curr.next;
    curr.next = null;
    this.length--;

    return curr;
  }

  display() {
    if (!this.head) return null;

    let curr = this.head;
    while (curr) {
      console.log(curr.data);
      curr = curr.next;
    }
    console.log("Size of Queue : " + this.length);
  }
}

const queue = new Queue();

queue.enqueue(10);
queue.enqueue(20);
queue.enqueue(30);
queue.enqueue(40);
queue.enqueue(50);

const removed = queue.dequeue();

console.log(removed, typeof removed);

// queue.display()
