//---pivot is last---//
function qS(arr){
    if(arr.length < 2) return arr

    const pivot = arr[arr.length-1]
    let leftArr = [] 
    let rightArr = []
    
    for(let i=0;i<arr.length-1;i++){
        if(arr[i] < pivot){
            leftArr.push(arr[i])
        }else{
            rightArr.push(arr[i])
        }
    }

    return [...qS(leftArr), pivot, ...qS(rightArr)]
}

//---pivot is at mid and Decending order---//

function quickSort(arr){
    if(arr.length < 2) return arr

    const pivot = arr[Math.floor(arr.length/2)]
    let leftArr = []
    let rightArr = []

    for(let i=0; i<arr.length; i++){
        if(arr[i] > pivot ){
            leftArr.push(arr[i])
        }else if(arr[i] < pivot){
            rightArr.push(arr[i])
        }
    }

    return [...quickSort(leftArr), pivot, ...quickSort(rightArr)]
}

//---pivot is at start---//

function sort(arr){
    if(arr.length <= 1) return arr

    const pivot = arr[0]
    let leftArr = []
    let rightArr = []

    for(let el of arr.slice(1,arr.length)){
        if(el < pivot) leftArr.push(el)
        if(el >= pivot) rightArr.push(el)
    }

    return [...sort(leftArr), pivot, ...sort(rightArr)]
}

const array = [2,5,1,3,6,8]

console.log(sort(array))