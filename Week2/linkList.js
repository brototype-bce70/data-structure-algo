
class Node{
    constructor(data){
        this.data = data
        this.next = null
    }
}

class List{
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }

    toTail(data){
        const newNode = new Node(data)
        if(!this.head){
            this.head = newNode
            this.tail = newNode
        }else{
            this.tail.next = newNode
            this.tail = newNode
        }
        this.size++
    }

    arrTo(arr){
        for(let i=0;i<arr.length;i++){
            this.toTail(arr[i])
        }
    }

    display(){
        if(!this.head) return "Empty"

        let current = this.head
        let res = ""
        while(current){
            res += current.data + " "
            current = current.next
        }
        return res
    }
}

const array = [243, 45, 23, 356, 3, 5346, 35, 5]

const list = new List()

list.arrTo(array)
// console.log(list.display());
