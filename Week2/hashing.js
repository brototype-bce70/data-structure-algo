
class HashTable {
    constructor(size){
        this.table = new Array(size)
    }

    _hash(key){
        let hash = 0
        for(let i=0 ; i<key.length ; i++){
            hash += key.charCodeAt(i)
        }
        return hash % this.table.length
    }

    set(key , value){
        const index = this._hash(key)

        if(this.table[index]){
            for(let i=0 ; i<this.table[index].length ; i++){
                if(this.table[index] [i] [0] === key){
                    this.table[index] [i] [1] = value
                    return
                }
            }
            this.table[index].push([key , value])
        }else{
            this.table[index] = []
            this.table[index].push([key , value])
        }
    }

    get(key){
        const index = this._hash(key)

        if(this.table[index]){
            for(let i=0 ; i<this.table[index].length ; i++){
                if(this.table[index] [i] [0] === key){
                    return this.table[index] [i] [1]
                }
            }
        }
        return undefined
    }

    rm(key){
        const index = this._hash(key)

        if(this.table[index]){
            for(let i=0 ; i<this.table[index].length ; i++){
                if(this.table[index] [i] [0] === key){
                    this.table[index].splice(i , 1)
                    return true
                }
            }
        }
        return false
    }

    display(){
        for(let i=0 ; i<this.table.length ; i++){
            if(this.table[i]){
                console.log("index ", i , " : ");
                console.log(this.table[i]);
            }
        }
    }
}

const hh = new HashTable(12)

// console.log(hh._hash("mane"))

hh.set("name" , "arjun")
hh.set("mane" , "alwin")
hh.set("age" , 22)
hh.set("place" , "ekm")
hh.set("dis" , "kl")
hh.set("mane" , "arun")


// console.log(hh.rm("name"));
// console.log(hh.get("name"));
hh.display()