function bubbleSort(arr) {
  let length = arr.length;

  for (let i = 0; i < length; i++) {
    for (let j = 0; j < length - i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        let temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
  return arr;
}

// const array = [8,10,2,5,3,10,20]

// console.log(bubbleSort(array));

//---Optimized Way---//

function bubbSort(arr) {
  let isSwapped;

  for (let i = 0; i < arr.length; i++) {
    isSwapped = false;
    for (let j = 0; j < arr.length - i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        var temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
        isSwapped = true;
      }
    }
    if (!isSwapped) break;
  }
  return arr;
}

// const array = [243, 45, 5, 67, 94, 1]

// console.log(bubbSort(array));

//---Find the greatest 3 numbers in array---//

function gorder(arr) {
  let lastIndex = arr.length - 1;
  let res = [];
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < lastIndex - i; j++) {
      if (arr[j] > arr[j + 1]) {
        var temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
    res.push(arr[lastIndex - i]);
  }
  return res;
}

const array = [8, 100, 2, 5, 3, 10, 20];

console.log(gorder(array));
