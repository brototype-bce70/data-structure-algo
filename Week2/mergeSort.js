//---Ascending order---//
function mergeSort(arr){
    if(arr.length < 2) return arr

    const mid = Math.floor(arr.length / 2)
    const leftArr = arr.slice(0, mid)
    const rightArr = arr.slice(mid)

    return merge(mergeSort(leftArr),mergeSort(rightArr))
    
}

function merge(leftArr,rightArr){
    const res = []

    while(leftArr.length && rightArr.length){
        if(leftArr[0] < rightArr[0]){
            res.push(leftArr.shift())
        }else{
            res.push(rightArr.shift())
        }
    }

    return [...res, ...leftArr, ...rightArr]
}

//---Dcending Order---//
function mgSrt(arr){
    if(arr.length < 2) return arr
    
    let middle = Math.floor(arr.length / 2)
    let lArr = arr.slice(0, middle)
    let rArr = arr.slice(middle)

    return join(mgSrt(lArr),mgSrt(rArr))
}

function join(lArr,rArr){
    const out = []

    while(lArr.length && rArr.length){
        if(lArr[0] > rArr[0]){
            out.push(lArr.shift())
        }else{
            out.push(rArr.shift())
        }
    }

    return [...out, ...lArr, ...rArr]
}



function sort(array){
    if(array.length < 2) return array

    const mid = Math.floor(array.length / 2)
    const leftArray = array.slice(0, mid)
    const rightArray = array.slice(mid)

    // console.log(array);
    // console.log(leftArray);
    // console.log(rightArray);
    return inn(sort(leftArray),sort(rightArray))

    

}

function inn(leftArray, rightArray){
    const res = []

    while(leftArray.length && rightArray.length){
        if(leftArray[0] < rightArray[0]){
            res.push(leftArray.shift())
        }else{
            res.push(rightArray.shift())
        }
    }

    return [...res, ...leftArray, ...rightArray]
}

const array = [2,7,4,1,6,3]

console.log(sort(array));
