class Stack{
    constructor(){
        this.item = []
    }

    push(data){
        this.item.push(data)
    }

    pop(){
        return this.item.pop()
    }

    peek(){
        return this.item[this.item.length-1]
    }

    isEmpty(){
        return this.item.length == 0 ? true : false
    }

    size(){
        return this.item.length
    }

    print(){
        console.log(this.item.toString());
    }
}

const obj = new Stack()


obj.push(10)
obj.push(20)
obj.push(50)

obj.print()
console.log(obj.pop());