function selSort(arr){
    for(let i=0;i<arr.length-1;i++){
        for(let j=i+1;j<arr.length;j++){
            if(arr[i]>arr[j]){
                var temp = arr[i]
                    arr[i] = arr[j]
                    arr[j] = temp
            }
        }
    }
    return arr
}

function selSortNew(arr){

    for(let i=0;i<arr.length-1;i++){
        let minVal = arr[i]
        for(let j=i+1;j<arr.length;j++){
            if(minVal>arr[j]){
                minVal = arr[j]
                arr[j] = arr[i]
                arr[i] = minVal
            }
        }
    }
    return arr
}

function odd(arr){
    let res = ""
    for(let i=0;i<arr.length;i++){
        for(let j=i+1;j<arr.length;j++){
            if(arr[i]>arr[j]){
                var temp = arr[j]
                    arr[j] = arr[i]
                    arr[i] = temp
            }
        }
            if(arr[i]%2 != 0){
                res += arr[i] + " "
            }
    }
    return res
}

const array = [88,653,5,67,1,8,2,10]

console.log(selSort(array));